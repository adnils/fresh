# fresh
Dotfiles and Ansible Playbooks to configure a fresh OS X or Linux install.

## Included
* prezto /w config
* vim /w config

## Instructions

Set shell to zsh
```
chsh -s /bin/zsh
```

Clone fresh
```
git clone https://github.com/adnils/fresh.git ~/fresh
```

Run playbook
```
cd ~/fresh && ansible-playbook site.yml -i inv
```

Launch vim
```
vim
```

Install vim plugins
```
:PluginInstall
```

*Done!*
